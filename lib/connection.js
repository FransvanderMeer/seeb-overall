
var connection = function(stream, type, controller, protocol, options) {
  
  this.stream = stream;
  this.stream.setTimeout(0);
  this.stream.setEncoding('utf8');
  this.stream.setKeepAlive(3000);


  this.type = type;
  this.controller = controller;
  this.protocol = protocol;
  this.connected = null;
  this._dataBuffer = "";
  this._pingTime = [];
  this.events = {};
  
  var self = this;

  if (this.type == 'server') {
    this.status = {"remote": this.stream.remoteAddress, "remotePort": this.stream.remotePort, "up": false, "avgPingTime": -1, "pingLost": 0, "type": "client"};
  } else {
    this.status = {"remote": options.host, "remotePort": options.port, "up": false, "avgPingTime": -1, "pingLost": 0, "type": "server"};
    require('dns').lookup(options.host, function (err, addr) {
      if (!err) {
        self.status.remote = addr;
      }
    });
  }

  
  this.addDefaultHandlers();
  this.addDefaultProtocol();
  
  if (this.type == 'client') {
    this.handShake();
    this.ping();
  }
  
}

connection.prototype.addListener = function (event, callback) {
  this.stream.on(event, callback);
}

connection.prototype.addDefaultHandlers = function() {
  var self = this;
  
  this.stream.on('data', function(data) {
    self.stream.pause();
    self._dataBuffer += data.toString('utf8');
    do {
      var dot = self._dataBuffer.indexOf('.');
      var len = 0;
      if (dot > 0) {
        len = parseInt(self._dataBuffer.substring( 0, dot ));
      }
      if (dot > 0 && self._dataBuffer.length >= len + 1 + dot) {
        var cmd = self._dataBuffer.substring( 1 + dot, len + 1 + dot );
        self._dataBuffer = self._dataBuffer.substring( len + 1 + dot );
        self.stream.emit('command', cmd);
      } else {
        break;
      }
    }
    while ( true );
    self.stream.resume();
  });
  
  this.stream.on('command', function(command) {
    self.receiveCommand(command);
  });
  
  this.stream.on('pong', function(ts) {
    var t = (((new Date).getTime()) - ts);
    self.status.pingLost--;
    self._pingTime.push(t);
    self._pingTime = self._pingTime.slice(-10);
    for(var i=0,sum=0;i<self._pingTime.length;sum+=self._pingTime[i++]);
    self.status.avgPingTime = parseFloat(sum / self._pingTime.length);
    self.sendCommand('setStatusSpeed', self.status.avgPingTime, self.status.pingLost);
  });
  
  this.stream.on('handShake', function() {
    self.controller.addConnection(self);
  });
  
  this.stream.on('down', function(e) {
    self.controller.removeConnection(self);
  });
  
  var up = function up() {
    if (!self.status.up) {
      self.status.up = true;
      this.emit('up');
    }
  }
  var down = function() {
    if (self.status.up) {
      self.status.up = false;
      this.emit('down');
      this.end();
    }
  }
  
  this.stream.on('error', down);
  this.stream.on('close', down);
  this.stream.on('end', down);
  
  this.stream.on('started', up);
  
  this.stream.emit('started');

}

connection.prototype.addDefaultProtocol = function() {
  if (typeof this.protocol.errorHandler == 'undefined') {
    this.protocol.errorHandler = function(error, cmd, args) {
      this.stream.emit('error', error);
      console.log(cmd + ' exits with error:', error);
    }
  }
  
  if (typeof this.protocol.ping == 'undefined') {
    this.protocol.ping = function(ts) {
      this.sendCommand('pong', ts);
    }
  }
  
  if (typeof this.protocol.pong == 'undefined') {
    this.protocol.pong = function(ts) {
      this.stream.emit('pong', ts);
    }
  }
  
  if (typeof this.protocol.setStatusSpeed == 'undefined') {
    this.protocol.setStatusSpeed = function(avgPingTime, pingLost) {
      this.status.avgPingTime = avgPingTime;
      this.status.pingLost = pingLost;
    }
  }

  if (typeof this.protocol.handShake == 'undefined') {
    this.protocol.handShake = function(connected) {
      this.connected = connected;
      if (typeof this.controller.handShakeContent == 'function') {
        this.sendCommand('hiFriend', this.controller.handShakeContent());
      }
      this.stream.emit('handShake');
    }
  }
  
  if (typeof this.protocol.hiFriend == 'undefined') {
    this.protocol.hiFriend = function(connected) {
      this.connected = connected;
      this.stream.emit('handShake');
    }
  }
  
}

connection.prototype.sendCommand = function () {
  if (this.stream.writable ) {
    str = JSON.stringify(arguments);
    this.stream.write(str.length + '.' + str);
  }
}

connection.prototype.receiveCommand = function (jsn) {
  try {
    var arg = JSON.parse(jsn);
  } catch (e) {
    var arg = [];
  }
  
  var argArr = [];
  for (x in arg) {
    argArr.push(arg[x]);
  }
  var callback = argArr.shift();
  try {
    this.protocol[callback].apply(this, argArr);
  } catch (ex) {
    this.sendCommand('errorHandler', ex.toString(), callback, argArr);
  }
}

connection.prototype.ping = function() {
  this.status.pingLost++;
  this.sendCommand('ping', ((new Date).getTime()));
}

connection.prototype.handShake = function() {
  if (typeof this.controller.handShakeContent == 'function') {
    this.sendCommand('handShake', this.controller.handShakeContent());
  }
}

connection.prototype.sendEmit = function(event) {
  var eventData = event[event.length] || event[event.length-1];
  this.events[eventData.uuid] = event;
  this.sendCommand('emitRequest', eventData.uuid);
  this.stream.emit('emitRequestSent', eventData.uuid);
}

connection.prototype.write = function () {
  this.stream.write.apply(this.stream, arguments);
}

connection.prototype.end = function() {
  this.stream.end();
  delete this;
}

module.exports = connection;
