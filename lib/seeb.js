var ee = require('eventemitter2').EventEmitter2, 
    util = require('util'), 
    fs = require('fs'),
    uuid = require('node-uuid'),
    os = require('os'),
    path = require('path'),
    dns = require('dns');


var sEEB = function(config) {
  
  // Load nconf with values  
  this.nconf.env().argv();
  this.nconf.overrides(config);

  // Load config.json
  configFile = path.resolve('./config.json');
  if (this.nconf.get('config')) {
    configFile = path.resolve(this.nconf.get('config'));
  }
  if (fs.existsSync(configFile)) {
    this.nconf.file(configFile);
  }

  try {
    this.nconf.defaults (
      JSON.parse(
        fs.readFileSync(__dirname + '/config.default.json', 'utf8')
      )
    );
  } catch (ex) { throw ex; }
  
  this.key = null;
  this.cert = null;
  this.ca = null;
  if (this.nconf.get('server:keyFile')) {
    this.key = fs.readFileSync(this.nconf.get('server:keyFile'));
  }
  if (this.nconf.get('server:certFile')) {
    this.cert = fs.readFileSync(this.nconf.get('server:certFile'));
  }
  if (this.nconf.get('server:caFile')) {
    this.ca = fs.readFileSync(this.nconf.get('server:caFile'));
  }
  
  this.clientKey = null;
  this.clientCert = null;
  this.clientCa = null;
  this.tlsServerName = this.nconf.get('client:tlsServerName');
  if (this.nconf.get('client:keyFile')) {
    this.clientKey = fs.readFileSync(this.nconf.get('client:keyFile'));
  }
  if (this.nconf.get('client:certFile')) {
    this.clientCert = fs.readFileSync(this.nconf.get('client:certFile'));
  }
  if (this.nconf.get('client:caFile')) {
    this.clientCa = fs.readFileSync(this.nconf.get('client:caFile'));
  }

  // Call the parent constructor
  ee.call(this, {
    wildcard: true, // should the event emitter use wildcards.
    delimiter: this.nconf.get('event-delimiter'), // the delimiter used to segment namespaces, defaults to `::`.
    maxListeners: 20, // the max number of listeners that can be assigned to an event, defaults to 10.
  });
  
  this.uuid = this.nconf.get('uuid');
  if (!this.uuid) {
    try {
      this.uuid = fs.readFileSync(__dirname + '/../cache/UUID', 'utf8');
    } catch (ex) { }
  }
  if (!this.uuid) {
    this.uuid = uuid.v4();
    try {
      fs.writeFile(__dirname + '/../cache/UUID', this.uuid, 'utf8');
    } catch (ex) { }
  }
  this.name = this.nconf.get('name') || this.uuid;
  this.hostname = this.nconf.get('hostname') || os.hostname();
  
  var networkInterfaces = os.networkInterfaces();
  this.addresses = [];
  for (x in networkInterfaces) {
    for (a in networkInterfaces[x]) {
      this.addresses.push(networkInterfaces[x][a]);
    }
  }

  this.emit("self" + this.nconf.get('event-delimiter') + "bootstrap");
  
  var self = this;
  
  // Start server on connection lost.
  this.on('self' + this.nconf.get('event-delimiter') + 'connection' + this.nconf.get('event-delimiter') + 'remove', function(uuid) {
    if (typeof self.server != 'undefined' && typeof self.server.getStatus == 'function') {
      status = self.server.getStatus();
      if (!status.up && status.fallback && uuid == status.fallbackfor) {
        self.startServer();
      }
    }
  });
  
  // Add hubs from remote
  this.on('self' + this.nconf.get('event-delimiter') + 'connection' + this.nconf.get('event-delimiter') + 'add', function(uuid) {
    var conn = self.connections[uuid];
    for (x in conn.connected.hubs) {
      self.addHub(conn.connected.hubs[x]);
    }
  });
  
  // Answer online?
  this.on('**' + this.nconf.get('event-delimiter') + 'online?', function() {
    var answer = {
      "name": self.name,
      "uuid": self.uuid,
      "clients": [],
      "servers": []
    }
    for(x in self.connections) {
      if (self.connections[x].type == 'client') {
        answer.servers.push(x);
      } else {
        answer.clients.push(x);
      }
    }
    self.emit('online!', answer);
  });
};

util.inherits(sEEB, ee);

sEEB.prototype.server = undefined;
sEEB.prototype.clients = {};
sEEB.prototype.hubs = {};
sEEB.prototype.eventsDone = {};
sEEB.prototype.eventsCache = {};
sEEB.prototype.connections = {};
sEEB.prototype.nconf = require('nconf');

sEEB.prototype.addHub = function(ip, entryhost) {
  if (typeof entryhost == 'undefined') {
    entryhost = false;
  }
  if (typeof ip == 'undefined' || ip == 'undefined') {
    return;
  }
  var localhostRE = /^(127\.[0-9]+\.[0-9]+\.[0-9]+|::1|fe80(:1)?::1(%.*)?)$/i;
  if (!this.isHub(ip)) {
    this.hubs[ip] = {
      "ip": ip,
      "entryhost": entryhost,
      "connected": false,
      "canconnect": false,
      "localhost": localhostRE.test(ip),
      "connecting_timestamp": 0,
      "connecting_attempts": 0
    };
  }
}

sEEB.prototype.getHub = function(ip) {
  if (this.isHub(ip)) {
    return this.hubs[ip];
  }
  return false;
}

sEEB.prototype.isHub = function(ip) {
  return (typeof this.hubs[ip] != 'undefined');
}

sEEB.prototype.setHubValue = function(ip, settings) {
  if (this.isHub(ip)) {
    for (x in settings) {
      this.hubs[ip][x] = settings[x];
    }
    return true;
  }
  return false;
}
  
sEEB.prototype.addConnection = function(connection) {
  this.addHub(connection.status.remote);
  if (connection.type == 'client') {
    this.setHubValue(connection.status.remote, {"canconnect": true, "connecting_attempts": 0});
  }
  if (connection.connected.name == 'networkconnection') {
    // Network connections are not event connections.
    // We don't control them.
    return;
  }
  var uuid = null;
  if (typeof connection.connected == 'object' && connection.connected != null && typeof connection.connected.uuid != 'undefined') {
    uuid = connection.connected.uuid;
  }
  if (uuid == null || uuid.length < 1) {
    return;
  }
  
  if (this.uuid == uuid) {
    // We are connected to ourselves... that is stupid!
    this.setHubValue(connection.status.remote, {"localhost": true});
    connection.end();
    return;
  }
  if (typeof this.connections[uuid] != 'undefined') {
    // We are connected twice... that is stupid!
    connection.end();
    return;
  }
  this.connections[uuid] = connection;
  this.emit('self' + this.nconf.get('event-delimiter') + 'connection' + this.nconf.get('event-delimiter') + 'add', connection.connected.uuid);
  
  // Empty cache when we have one.
  if (this.eventsCache.length) {
    for (x in this.eventsCache) {
      connection.sendEmit(this.eventsCache[x]);
      delete this.eventsCache[x];
    }
  }
}

sEEB.prototype.removeConnection = function(connection) {
  if (typeof connection.connected == 'object' && connection.connected != null && typeof connection.connected.name != 'undefined' && connection.connected.name == 'networkconnection') {
    // Network connections are not event connections.
    // We don't control them.
    return;
  }
  var uuid = null;
  if (typeof connection.connected == 'object' && connection.connected != null && typeof connection.connected.uuid != 'undefined') {
    uuid = connection.connected.uuid;
  }
  if (uuid == null || uuid.length < 1) {
    return;
  }
  if (typeof this.connections[uuid] != 'undefined' && connection.type == this.connections[uuid].type) {
    try {
      this.connections[uuid].end();
    } catch (ex) {}
    this.emit('self' + this.nconf.get('event-delimiter') + 'connection' + this.nconf.get('event-delimiter') + 'remove', connection.connected.uuid);
    delete this.connections[uuid];
    this.balanceMesh();
  }
}

sEEB.prototype.isNewEvent = function(uuid) {
  return (typeof this.eventsDone[uuid] === 'undefined');
}

sEEB.prototype.emit = function() {
  if (
      ['newListener', 'removeListener', 'removeAllListeners'].indexOf(arguments[0]) !== -1 ||
      arguments[0].indexOf('self' + this.nconf.get('event-delimiter')) === 0
     ) {
    // Emit these events only locally
    return ee.prototype.emit.apply(this, arguments);
  }
  var lastArgument = arguments[arguments.length - 1];
  if (typeof lastArgument.type != 'undefined' && lastArgument.type.indexOf('seeb.') === 0) {
    var eventDetails = lastArgument;
    if (!this.isNewEvent(eventDetails.uuid)) {
      // Get out, we already done this event.
      return;
    }
  } else {
    // Prepend event.
    arguments[0] = this.hostname + this.nconf.get('event-delimiter') + this.name + this.nconf.get('event-delimiter') + arguments[0];
    // Add extra argument with the details.
    var eventDetails = {
      type: 'seeb.1',
      uuid: uuid.v4(),
      timestamp: (new Date).getTime(),
      sender: this.uuid,
      payload: (typeof arguments[1] !== 'undefined'),
      via: [],
      hops: 0
    };
    arguments[arguments.length] = eventDetails;
  }
  // Mark event as done.
  this.eventsDone[eventDetails.uuid] = eventDetails.timestamp;

  // Always emit locally
  ee.prototype.emit.apply(this, arguments);

  if ( ( this.nconf.get('server:active') || this.nconf.get('client:active') ) && this.connections.length == 0) {
    // We have no connections, but we should have... lets cache this
    // Can be that we are first/last on the network? TODO: find a way to detect that.
    this.eventsCache[eventDetails.uuid] = arguments;
  }
  
  // Emit on connections.
  for (x in this.connections) {
    if (eventDetails.via.indexOf(x) < 0) { // Do not send to nodes with that are in via array. They already have this.
      this.connections[x].sendEmit(arguments);
    }
  }
  
  // Garbage collect in 20% of the calls.
  if (Math.random() < 0.2) {
    this.gc();
  }
  
  return true;
};

sEEB.prototype.start = function(cb) {
  var self = this;
  this.startServer();
  this.startClients();
  this.iv = setInterval(function() {
    self.balanceMesh();
  }, 60000);
  if (cb && typeof cb === 'function') {
    cb();
  }
}

sEEB.prototype.startServer = function() {
  if (this.nconf.get('server:active')) {
    sEEBserver = require(__dirname + '/seeb-server');
    conf = {
      port: this.nconf.get('port'),
      tls: this.nconf.get('tls'),
      host: this.nconf.get('server:bind-to'),
      key: this.key,
      cert: this.cert,
      ca: this.ca || [this.clientCert],
      keyAuth: this.nconf.get('keyAuth')
    }
    this.emit('self' + this.nconf.get('event-delimiter') + 'startServer', conf.port);
    this.server = new sEEBserver(conf, this);
    this.server.start();
  }
  return;
}

sEEB.prototype.startClients = function() {
  if (!this.nconf.get('client:active')) {
    return;
  }
  var hubs = this.nconf.get('client:entry-hosts');
  for (var i = 0; i< hubs.length; i++) {
    var client = this.startClient(hubs[i], true);
  }
  return;
}

sEEB.prototype.startClient = function(host, entryhost) {
  var self = this;
  dns.lookup(host, function (err, addr) {
    if (!err) {
      if (!self.isHub(addr)) {
        self.addHub(addr, entryhost);
      }
      var hub = self.getHub(addr);
      if (
          (hub.canconnect && ((new Date).getTime() - 60000) > hub.connecting_timestamp) ||
          (!hub.canconnect && ((new Date).getTime() - 60000) > hub.connecting_timestamp && hub.connecting_attempts < 5) ||
          (!hub.canconnect && ((new Date).getTime() - 3600000) > hub.connecting_timestamp && hub.connecting_attempts < 15)
         ) {
        hub.connecting_attempts++;
        self.setHubValue(addr, {"connecting_timestamp": (new Date).getTime(), "connecting_attempts": hub.connecting_attempts});
        return self.connectClient(addr);
      }
    }
  });
}

sEEB.prototype.connectClient = function(ip, controller) {
  if (typeof controller == 'undefined') {
    controller = this;
  }
  // avoid double connections
  if (typeof controller.connections != 'undefined') {
    for (x in controller.connections) {
      if (controller.connections[x].status.remote == ip && controller.connections[x].type == 'client') {
        return controller.connections[x];
      }
    }
  }
  
  sEEBclient = require(__dirname + '/seeb-client');
  var conf = {
    port: this.nconf.get('port'),
    tls: this.nconf.get('tls'),
    host: ip,
    key: this.clientKey,
    cert: this.clientCert,
    ca: this.clientCa || [this.cert],
    keyAuth: this.nconf.get('keyAuth'),
    tlsServerName: this.tlsServerName
  }
  this.emit('self' + this.nconf.get('event-delimiter') + 'connectClient', ip);
  return new sEEBclient(conf, controller);
}

sEEB.prototype.handShakeContent = function() {
  var conn = [];
  for(x in this.connections) {
    conn.push([x, this.connections[x].status]);
  }
  var content = {
    'hostname': this.hostname,
    'name': this.name,
    'uuid': this.uuid,
    'connections': conn,
    'hubs': Object.keys(this.hubs)
  };
  return content;
}

sEEB.prototype.balanceMesh = function() {
  if (!this.nconf.get('client:active')) {
    // Useless if we cannot act as client.
    return;
  }
  this.emit('self' + this.nconf.get('event-delimiter') + 'balanceMesh');
  // Connect at least to one entry hub.
  for (x in this.hubs) {
    this.hubs[x].connected = false;
  }

  var numConnectionsEntryHosts = 0;
  var numConnections = 0;
  var numConnectionsServer = 0;
  for (x in this.connections) {
    var connected = this.connections[x];
    if (!this.isHub(connected.status.remote)) {
      this.addHub(connected.status.remote);
    }
    var hub = this.getHub(connected.status.remote);
    this.setHubValue(connected.status.remote, {"connected": true});
    if (hub.entryhost) {
      numConnectionsEntryHosts++;
    }
    if (connected.type == 'server') {
      numConnectionsServer++;
    }
    numConnections++;
  }
  
  if (numConnectionsEntryHosts < 1) {
    // Panic.. we might not be connected to the network.
    this.startClients();
  }
  
  if (this.nconf.get('client:redundancy') > numConnections) {
    // just connect to all hubs we can.
    for (x in this.hubs) {
      if (!this.hubs[x].connected) {
        this.startClient(x);
        numConnections++;
        if (this.nconf.get('client:redundancy') < numConnections) {
          break;
        }
      }
    }
  }
}

// Garbage Collect.
sEEB.prototype.gc = function() {
  this.emit('self' + this.nconf.get('event-delimiter') + 'gc');
  var ts = (new Date).getTime() - (parseInt(this.nconf.get('emit-ttl')) * 1000);
  for (x in this.eventsDone) {
    if (this.eventsDone[x] < ts) {
      delete this.eventsDone[x];
    }
  }
  for (x in this.eventsCache) {
    var lastArgument = this.eventsCache[x][this.eventsCache[x].length - 1];
    if (lastArgument.timestamp < ts) {
      delete this.eventsCache[x];
    }
  }
}

module.exports = sEEB;
