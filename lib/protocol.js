
protocol = {};

protocol.emitRequest = function(uuid) {
  if (this.controller.isNewEvent(uuid)) {
    this.sendCommand('emitDataRequest', uuid);
  } else {
    this.sendCommand('emitDataDeny', uuid);
  }
}

protocol.emitDataRequest = function(uuid) {
  var event = this.events[uuid];
  this.sendCommand('emitData', event);
  delete this.events[uuid];
}

protocol.emitDataDeny = function(uuid) {
  delete this.events[uuid];
}

protocol.emitData = function(event) {
  // Create real array, or apply will deny...
  var eventArr = [];
  for (x in event) {
    eventArr[x] = event[x];
  }
  eventArr[x].hops++;
  if (typeof this.connected != 'undefined' && this.connected != null && typeof this.connected.uuid != 'undefined') {
    var u = this.connected.uuid;
    eventArr[x].via.push(u);
  }
  this.controller.emit.apply(this.controller, eventArr);
}

module.exports = protocol;
