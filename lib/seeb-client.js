var connection = require(__dirname + '/connection'),
    protocol = require(__dirname + '/protocol');

var sEEB_client = function(config, controller) {
  
  var self = this;
  
  this.iv = null;
  this.connection = null;
  this.events = {};
  this.controller = controller;
  this.config = config;
  this.pingInterval = 0;
  this.pingCounter = 0;
  
  this.opt = {
    port: this.config.port,
    host: this.config.host || '127.0.0.1'
  };
  
  if (this.config.tls) {
    var net = require('tls');
    this.opt.key = this.config.key || undefined;
    this.opt.cert = this.config.cert || undefined;
    this.opt.ca = this.config.ca || undefined;
    this.opt.rejectUnauthorized = this.config.keyAuth || false;
    this.opt.servername = this.config.tlsServerName || undefined;
  } else {
    var net = require('net');
  }
  this.connection = new connection(net.connect(this.opt), 'client', this.controller, protocol, this.opt);
  
  this.connection.addListener('up', function() {
    self.restartPingInterval(1);
  });

  this.connection.addListener('down', function() {
    clearInterval(self.iv);
  });
  
  return this.connection;
}

sEEB_client.prototype.restartPingInterval = function (t) {
  var self = this;
  clearInterval(this.iv);
  this.iv = setInterval(function() {
    if (self.pingCounter > 9) {
      self.updatePingInterval(60);
    } else {
      self.pingCounter++;
    }
    self.connection.ping();
  }, t * 1000);
}

sEEB_client.prototype.updatePingInterval = function (t) {
  if (this.pingInterval != t) {
    this.pingInterval = t;
    this.restartPingInterval(t);
  }
}

module.exports = sEEB_client;
