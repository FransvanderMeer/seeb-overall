var connection = require(__dirname + '/connection'),
    protocol = require(__dirname + '/protocol');

var sEEB_server = function(config, controller) {
  this.controller = controller;
  this.opt = {};
  this.config = config;
  this.server = null;
  this.status = {"up": false, "fallback": false, "fallbackfor": ''}
  var self = this;
  if (this.config.tls) {
    var net = require('tls');
    this.opt.key = this.config.key || undefined;
    this.opt.cert = this.config.cert || undefined;
    this.opt.requestCert =  this.config.keyAuth || false;
    this.opt.rejectUnauthorized = this.config.keyAuth || false;
    this.opt.ca = this.config.ca || undefined;
  } else {
    var net = require('net');
  }
  
  this.server = net.createServer(this.opt, function(c) {
    var conn = new connection(c, 'server', self.controller, protocol);
  });
  
  this.server.on('listening', function() {
    self.setStatus({"up": true});
    self.controller.emit('self::server::running');
  });
  
  this.server.on('close', function() {
    self.setStatus({"up": false});
  });
  
  this.server.on('error', function (e) {
    self.setStatus({"up": false});
    if (e.code == 'EADDRINUSE') {
      // try to find out for who we can fallback.
      fbcontroller = {
        handShakeContent: function() {
          var content = {
            name: 'networkonnection'
          }
          return content;
        },
        addConnection: function(conn) {
          var uuid = conn.connected.uuid;
          conn.end();
          status = self.getStatus();
          if (false == status.fallback || status.fallbackfor != uuid) {
            self.setStatus({"fallback": true, "fallbackfor": uuid});
            self.controller.emit("self::server::fallbackready", uuid);
          }
        },
        removeConnection: function(conn) {}
      };
      host = self.config.host;
      if (host == '0.0.0.0') {
        host = '127.0.0.1';
      }
      self.controller.connectClient(host, fbcontroller);
      status = self.getStatus();
      if (false == status.fallback) {
        // We are not yet ready for fallback; timeout and try again
        // Note: This results in extra starts when something is wrong 
        //       with the server we connect to. Also there is always one
        //       extra startup attempt.
        t = setTimeout(function() {
          self.start();
        }, 500);
      } else {
        // We make a connection to our fallback
        if (self.controller.nconf.get('client:active')) {
          self.controller.connectClient(host);
        }
        self.controller.balanceMesh();
      }
    }
    self.controller.emit('self::server::error', e);
  });
  
}

sEEB_server.prototype.start = function() {
  this.server.listen(this.config.port, this.config.host);
  this.controller.emit('self::server::start')
}

sEEB_server.prototype.getStatus = function() {
  return this.status;
}

sEEB_server.prototype.setStatus = function(status) {
  for(x in status) {
    this.status[x] = status[x];
  }
}

module.exports = sEEB_server;
