var fs = require('fs'),
    uuid = require('node-uuid'),
    os = require('os'),
    path = require('path');


var sEEB_singleshot = function(config) {
  
  var self = this;
  process.on('exit', function() {
    if (self.setIsSend()) {
      process.exit(0);
    }
    console.log(self.setLastError());
    process.exit(1);
  });
  
  // Load nconf with values  
  this.nconf.env().argv();
  this.nconf.overrides(config);

  // Load config.json
  configFile = path.resolve('./config.json');
  if (this.nconf.get('config')) {
    configFile = path.resolve(this.nconf.get('config'));
  }
  if (fs.existsSync(configFile)) {
    this.nconf.file(configFile);
  }

  try {
    this.nconf.defaults (
      JSON.parse(
        fs.readFileSync(__dirname + '/config.default.json', 'utf8')
      )
    );
  } catch (ex) { throw ex; }
  
  this.key = null;
  this.cert = null;
  this.ca = null;
  this.serverCert = null;
  
  if (this.nconf.get('client:keyFile')) {
    this.key = fs.readFileSync(this.nconf.get('client:keyFile'));
  }
  if (this.nconf.get('client:certFile')) {
    this.cert = fs.readFileSync(this.nconf.get('client:certFile'));
  }
  if (this.nconf.get('client:caFile')) {
    this.ca = fs.readFileSync(this.nconf.get('client:caFile'));
  }
  if (this.nconf.get('server:certFile')) {
    this.serverCert = fs.readFileSync(this.nconf.get('server:certFile'));
  }

  this.uuid = this.nconf.get('uuid');
  if (!this.uuid) {
    try {
      this.uuid = fs.readFileSync(__dirname + '/../cache/UUID', 'utf8');
    } catch (ex) { }
  }
  if (!this.uuid) {
    this.uuid = uuid.v4();
    try {
      fs.writeFile(__dirname + '/../cache/UUID', this.uuid, 'utf8');
    } catch (ex) { }
  }
  this.name = this.nconf.get('name') || this.uuid;
  this.hostname = this.nconf.get('hostname') || os.hostname();
  
  this.event = this.nconf.get('event');
  
  if (typeof this.event == 'undefined') {
    throw new Error('No event received');
  }
  this.eventData = this.nconf.get('eventData') || (this.nconf.get('eventDataBase64')?new Buffer(this.nconf.get('eventDataBase64'), 'base64').toString('utf8'):undefined)
  try {
    conv = JSON.parse(this.eventData);
  } catch(e) {
    conv = this.eventData;
  }
  this.eventData = conv;
  
  if (typeof this.eventData != 'undefined' && !(this.eventData instanceof Array)) {
    this.eventData = [this.eventData];
  }
  
  if (this.eventData instanceof Array) {
    var arg = this.eventData;
  } else {
    var arg = [];
  }
  
  // Prepend event.
  arg.unshift(this.hostname + this.nconf.get('event-delimiter') + this.name + this.nconf.get('event-delimiter') + this.event);
  // Add extra argument with the details.
  var eventDetails = {
    type: 'seeb.1',
    uuid: uuid.v4(),
    timestamp: (new Date).getTime(),
    sender: this.uuid,
    payload: (typeof this.eventData !== 'undefined'),
    via: [],
    hops: 0
  };
  arg.push(eventDetails);
  this.isSend = false;
  this.lastError = '';
  
  var hubs = this.nconf.get('client:entry-hosts');
  for (var i = 0; i< hubs.length; i++) {
    this.sendEmit(hubs[i], arg);
  }

};

sEEB_singleshot.prototype.setIsSend = function (send) {
  if (typeof send != 'undefined') {
    this.isSend = send;
  }
  return this.isSend;
}
sEEB_singleshot.prototype.setLastError = function(e) {
  if (typeof e != 'undefined') {
    this.lastError = e;
  }
  return this.lastError;
}

sEEB_singleshot.prototype.sendEmit = function(host, eventData) {
  var opt = {
    port: this.nconf.get('port'),
    host: host
  };
  
  if (this.nconf.get('tls')) {
    var net = require('tls');
    opt.key = this.key;
    opt.cert = this.cert;
    opt.ca = this.ca||[this.serverCert];
    opt.rejectUnauthorized = this.nconf.get('keyAuth') || false;
    opt.servername = this.nconf.get('client:tlsServerName');
  } else {
    var net = require('net');
  }
  str = JSON.stringify(['emitData', eventData]);
  var self = this;
  var stream = net.connect(opt, function() {
    this.write(str.length + '.' + str);
    this.end();
    self.setIsSend(true);
  });
  stream.setTimeout(1);
  stream.setEncoding('utf8');
  stream.on('error', function(e) {
    self.setLastError(e.toString());
  });
}

sEEB_singleshot.prototype.nconf = require('nconf');


module.exports = sEEB_singleshot;
